const http = require('http');

const PORT = 4000;



http.createServer(function(request,response){
	if(request.url === "/" && request.method === "GET"){
		response.writeHead(200,{'Content-type': 'text/plain'})
		response.end('Welcome to Booking System')
	}
	else if (request.url ==="/profile" && request.method === "GET"){
		response.writeHead(200,{'Content-type': 'text/plain'})
		response.end('Welcome to your profile')
	}
	else if (request.url ==="/course" && request.method === "GET"){
		response.writeHead(200,{'Content-type': 'text/plain'})
		response.end("Here's our courses available")		
	}
	else if (request.url ==="/addcourse" && request.method === "POST"){
		response.writeHead(200,{'Content-type': 'text/plain'})
		response.end("Add a course to our resources")		
	}
	else if (request.url ==="/updateCourse" && request.method === "PUT"){
		response.writeHead(200,{'Content-type': 'text/plain'})
		response.end("Update a course to our resources")		
	}
	else if (request.url ==="/archiveCourse" && request.method === "DELETE"){
		response.writeHead(200,{'Content-type': 'text/plain'})
		response.end("Archive courses to our resources")		
	}
	else{
		response.writeHead(404,{'Content-type': 'text/plain'})
		response.end("Error 404: Page not found")
	}
}).listen(PORT)

console.log(`Server is running at localhost:${PORT}`)